#include <X11/XF86keysym.h>
/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 1;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft  = 0;   /* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int coloredstatusbar   = 1;     /* 0 means mono color status bar */
static const int selcoloredbar      = 1;     /* 0 means unfocused monitors' status bars will have SchemeNorm fg as base color, 1 SchemeSel fg */
static const int showbar            = 1;     /* 0 means no bar */
static const int showtagmonnums     = 1;     /* 0 means no "this tagset is displayed on monitor N" */
static const int hidewinlesstags    = 1;     /* 1 means that a tagset name is not drawn if it doesn't have windows on it */
static const int switchonempty      = 0;     /* 1: when moving monitor's last window to a tagset visible on another monitor change focus there */
static const int topbar             = 1;     /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=10" };
static const char dmenufont[]       = "monospace:size=16";
static const char col_darkgray[]    = "#111111";
static const char col_gray[]        = "#888888";
static const char col_lightgray[]   = "#cccccc";
static const char col_black[]       = "#000000";
static const char col_red[]         = "#FF0000";
static const char col_orange[]      = "#FFA500";
static char BarCol_Fg[]             = "#FFA500"; //!!overridden!! by parsed ANSI color codes
static char BarCol_Bg[]             = "#000000"; //!!overridden!! if coloredstatusbar == 1
static char BarCol_Border[]         = "#FF0000"; // unused
static const char *colors[][3]      = {
	/*               fg             bg         border        */
	[SchemeNorm] = { col_lightgray, col_black, col_gray      },
	[SchemeSel]  = { col_orange,    col_black, col_red       },
	/*               red            green      blue          */
	[BarCol]     = { BarCol_Fg,     BarCol_Bg, BarCol_Border },
};
static char *termcolors[]  = {
// normal (as in ESC [ 30-37 m)
	"#000000", // black
	"#C50F1F", // red
	"#25BC24", // green
	"#ADAD27", // yellow
	"#492EE1", // blue
	"#D338D3", // magenta
	"#33BBC8", // cyan
	"#CBCCCD", // white

// bright (as in ESC [ 90-97 m)
	"#818383", // black (gray)
	"#FC391F", // red
	"#31E722", // green
	"#EAEC23", // yellow
	"#3B78FF", // blue
	"#F935F8", // magenta
	"#14F0F0", // cyan
	"#FFFFFF", // white
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *    WM_CLASS(STRING) = instance, class
	 *    WM_NAME(STRING) = title
	 */
	/* class          instance  title  tags mask  isfloating  monitor */
	{ "steam",        NULL,     NULL,  1 << 1,    1,          -1 },
	{ "discord",      NULL,     NULL,  1 << 0,    0,          -1 },
	{ "Element",      NULL,     NULL,  1 << 0,    0,          -1 },

	{ "Navigator",    NULL,     NULL,  1 << 2,    0,          -1 },
	{ "floorp",       NULL,     NULL,  1 << 2,    0,          -1 },
	{ "firedragon",   NULL,     NULL,  1 << 2,    0,          -1 },
	{ "librewolf",    NULL,     NULL,  1 << 2,    0,          -1 },
	{ "firefox",      NULL,     NULL,  1 << 2,    0,          -1 },
	{ "Firefox",      NULL,     NULL,  1 << 2,    0,          -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "HHH",      grid },
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* startup commands */
void startupcmds()
{
	Monitor *m;
	int moncnt = 0;

	for (m = mons; m; m = m->next) moncnt++;

	switch (moncnt)
	{
		case 1:
			break;
		case 2:
			break;
		case 3:
			// default to 0 for 3rd monitor's master stack size
			mons->next->next->nmaster = 0;
			break;
		case 4:
			// default to 0 for 3rd monitor's master stack size
			mons->next->next->nmaster = 0;

			// default to fullscreen layout on 4th monitor
			m = mons->next->next->next;
			controllayout(&((Arg) { .i = 0 }), &((Arg) { .v = m }));
			//controllayout(ARG(.i = 0), ARG(.v = m));
			break;
		default:
			break;
	}

	return;
}


/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon,        // monitor
                                               "-fn", dmenufont,      // font
                                               "-nb", col_darkgray,   // unselected bg
                                               "-nf", col_lightgray,  // unselected text
                                               "-sb", col_black,      // selected bg
                                               "-sf", col_orange,     // selected text
                                               NULL };

static const char *roficmd[] = { "rofi", "-combi-modi", "run,drun", "-show", "combi", NULL };

static const char *term1cmd[] = { "kitty", NULL };
static const char *term2cmd[] = { "stterm", NULL };

static const char *maimselcmd[] = { "maim-crop",    NULL };
static const char *maimwincmd[] = { "maim-window",  NULL };
static const char *maimdescmd[] = { "maim-desktop", NULL };

//static const char *playcmd[]         = { "playerctl", "play",               NULL };
static const char *playcmd[]           = { "playerctl", "play-pause",         NULL };
static const char *pausecmd[]          = { "playerctl", "pause",              NULL };
static const char *stopcmd[]           = { "playerctl", "stop",               NULL };
static const char *prevcmd[]           = { "playerctl", "previous",           NULL };
static const char *nextcmd[]           = { "playerctl", "next",               NULL };
static const char *rewindcmd[]         = { "playerctl", "position", "5-",     NULL };
static const char *forwardcmd[]        = { "playerctl", "position", "5+",     NULL };
static const char *repeatcmd[]         = { "playerctl", "loop",     "toggle", NULL };
static const char *randomplaycmd[]     = { "playerctl", "shuffle,", "toggle", NULL };
//static const char *cycletrackcmd[]   = { "playerctl", "??", NULL };

static const char *volupcmd[]          = { "pactl", "set-sink-volume", "@DEFAULT_SINK@", "+5%",  NULL };
static const char *mutecmd[]           = { "pactl", "set-sink-mute", "@DEFAULT_SINK@", "toggle", NULL };
static const char *voldowncmd[]        = { "pactl", "set-sink-volume", "@DEFAULT_SINK@", "-5%",  NULL };

static const char *brightnessupcmd[]   = { "xbacklight", "-inc", "5%", NULL };
static const char *brightnessdowncmd[] = { "xbacklight", "-dec", "5%", NULL };

static const char *notifshowcmd[]   = { "dunstctl", "history-pop", NULL };
static const char *notifclosecmd[]  = { "dunstctl", "close-all", NULL };
static const char *notifclearcmd[]  = { "dunstctl", "history-clear", NULL };

static const char *mirror1cmd[]     = { "screen-mirror.sh", "HDMI-0",  NULL };
static const char *mirror3cmd[]     = { "screen-mirror.sh", "DP-3",    NULL };
static const char *mirror2cmd[]     = { "screen-mirror.sh", "DP-0",    NULL };
static const char *mirror0cmd[]     = { "screen-mirror.sh", "DVI-D-0", NULL };

static const char *lockscreencmd[]      = { "slock", NULL };
static const char *killswitchlockcmd[]  = { "slock-killswitch", NULL };

static const char *hibernatecmd[]   = { "systemctl", "hibernate", NULL };
static const char *rebootcmd[]      = { "systemctl", "reboot",    NULL };
static const char *suspendcmd[]     = { "systemctl", "suspend",   NULL };
static const char *poweroffcmd[]    = { "systemctl", "poweroff",  NULL };

static const Key keys[] = {
	/* modifier                     key                             function                argument */
	{ MODKEY|ShiftMask,             XK_Return,                      spawn,                  {.v = term1cmd } },
	{ MODKEY|ControlMask|ShiftMask, XK_Return,                      spawn,                  {.v = term2cmd } },
	{ MODKEY,                       XK_c,                           spawn,                  {.v = term1cmd } },
	{ MODKEY|ControlMask,           XK_c,                           spawn,                  {.v = term2cmd } },
	{ MODKEY|ShiftMask,             XK_c,                           killclient,             {0} },
	{ MODKEY,                       XK_f,                           spawn,                  {.v = roficmd } },
	{ MODKEY,                       XK_p,                           spawn,                  {.v = dmenucmd } },
	TAGKEYS(                        XK_1,                                                   0)
	TAGKEYS(                        XK_2,                                                   1)
	TAGKEYS(                        XK_3,                                                   2)
	TAGKEYS(                        XK_4,                                                   3)
	TAGKEYS(                        XK_5,                                                   4)
	TAGKEYS(                        XK_6,                                                   5)
	TAGKEYS(                        XK_7,                                                   6)
	TAGKEYS(                        XK_8,                                                   7)
	TAGKEYS(                        XK_9,                                                   8)
	{ MODKEY,                       XK_0,                           view,                   {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,                           tag,                    {.ui = ~0 } },
	{ MODKEY,                       XK_space,                       controllayoutw,         {.i =  0 } },
	{ MODKEY|ShiftMask,             XK_space,                       controllayoutw,         {.i =  1 } },
	{ MODKEY,                       XK_i,                           togglebar,              {0} },
	{ MODKEY|ShiftMask,             XK_i,                           toggleshowtagmonnums,   {0} },
	{ MODKEY,                       XK_t,                           togglefloating,         {0} },
	{ MODKEY,                       XK_Tab,                         controllayoutw,         {.i =  2 } },
	{ MODKEY,                       XK_k,                           focusstack,             {.i = +1 } },
	{ MODKEY,                       XK_j,                           focusstack,             {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_k,                           swapinstack,            {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_j,                           swapinstack,            {.i = -1 } },
	{ MODKEY,                       XK_h,                           setmfact,               {.f = -0.001} },
	{ MODKEY,                       XK_l,                           setmfact,               {.f = +0.001} },
	{ MODKEY,                       XK_o,                           setmfact,               {.f =  0.000} },
	{ MODKEY|ShiftMask,             XK_h,                           setcfact,               {.f = -0.025} },
	{ MODKEY|ShiftMask,             XK_l,                           setcfact,               {.f = +0.025} },
	{ MODKEY|ShiftMask,             XK_o,                           setcfact,               {.f =  0.000} },
	{ MODKEY,                       XK_comma,                       incnmaster,             {.i = +1 } },
	{ MODKEY,                       XK_period,                      incnmaster,             {.i = -1 } },
	{ Mod1Mask,                     XK_Tab,                         cyclemon,               {.i = -1 } },
	{ Mod1Mask|ShiftMask,           XK_Tab,                         cyclemon,               {.i = +1 } },
	{ MODKEY,                       XK_r,                           focusmon,               {.i =  0 } },
	{ MODKEY,                       XK_e,                           focusmon,               {.i =  1 } },
	{ MODKEY,                       XK_w,                           focusmon,               {.i =  2 } },
	{ MODKEY,                       XK_q,                           focusmon,               {.i =  3 } },
	{ MODKEY|ShiftMask,             XK_r,                           sendtomon,              {.i =  0 } },
	{ MODKEY|ShiftMask,             XK_e,                           sendtomon,              {.i =  1 } },
	{ MODKEY|ShiftMask,             XK_w,                           sendtomon,              {.i =  2 } },
	{ MODKEY|ShiftMask,             XK_q,                           sendtomon,              {.i =  3 } },
	{ MODKEY|ControlMask,           XK_l,                           spawn,                  {.v = lockscreencmd } },
	{ MODKEY|ControlMask|ShiftMask, XK_l,                           spawn,                  {.v = killswitchlockcmd } },
	{ MODKEY|ControlMask|ShiftMask, XK_s,                           spawn,                  {.v = suspendcmd } },
	{ MODKEY|ControlMask|ShiftMask, XK_h,                           spawn,                  {.v = hibernatecmd } },
	{ MODKEY|ControlMask|ShiftMask, XK_k,                           quit,                   {0} },
	{ MODKEY|ControlMask|ShiftMask, XK_r,                           spawn,                  {.v = rebootcmd } },
	{ MODKEY|ControlMask|ShiftMask, XK_p,                           spawn,                  {.v = poweroffcmd } },
	{ MODKEY,                       XK_Print,                       spawn,                  {.v = maimdescmd } },
	{ MODKEY|ControlMask,           XK_Print,                       spawn,                  {.v = maimwincmd } },
	{ MODKEY|ShiftMask,             XK_Print,                       spawn,                  {.v = maimselcmd } },
	{ 0,                            XF86XK_AudioPlay,               spawn,                  {.v = playcmd } },
	{ 0,                            XF86XK_AudioPause,              spawn,                  {.v = pausecmd } },
	{ 0,                            XF86XK_AudioStop,               spawn,                  {.v = stopcmd } },
	{ 0,                            XF86XK_AudioPrev,               spawn,                  {.v = prevcmd } },
	{ 0,                            XF86XK_AudioNext,               spawn,                  {.v = nextcmd } },
	{ 0,                            XF86XK_AudioRewind,             spawn,                  {.v = rewindcmd } },
	{ 0,                            XF86XK_AudioForward,            spawn,                  {.v = forwardcmd } },
	{ 0,                            XF86XK_AudioRepeat,             spawn,                  {.v = repeatcmd } },
	{ 0,                            XF86XK_AudioRandomPlay,         spawn,                  {.v = randomplaycmd } },
//	{ 0,                            XF86XK_AudioCycleTrack,         spawn,                  {.v = cycletrackcmd } },
	{ 0,                            XF86XK_AudioLowerVolume,        spawn,                  {.v = voldowncmd } },
	{ 0,                            XF86XK_AudioMute,               spawn,                  {.v = mutecmd } },
	{ 0,                            XF86XK_AudioRaiseVolume,        spawn,                  {.v = volupcmd } },
	{ 0,                            XF86XK_MonBrightnessUp,         spawn,                  {.v = brightnessupcmd } },
	{ 0,                            XF86XK_MonBrightnessDown,       spawn,                  {.v = brightnessdowncmd } },
	{ MODKEY,                       XK_n,                           spawn,                  {.v = notifshowcmd } },
	{ MODKEY|ShiftMask,             XK_n,                           spawn,                  {.v = notifclosecmd } },
	{ MODKEY|ControlMask|ShiftMask, XK_n,                           spawn,                  {.v = notifclearcmd } },
	{ MODKEY|ControlMask,           XK_q,                           spawn,                  {.v = mirror1cmd } },
	{ MODKEY|ControlMask,           XK_w,                           spawn,                  {.v = mirror3cmd } },
	{ MODKEY|ControlMask,           XK_e,                           spawn,                  {.v = mirror2cmd } },
	{ MODKEY|ControlMask,           XK_r,                           spawn,                  {.v = mirror0cmd } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
//	{ ClkLtSymbol,          0,              Button1,        setlayoutw,     {0} },
//	{ ClkLtSymbol,          0,              Button3,        setlayoutw,     {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = term1cmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

